# Medidor CO2

## Resumen

Proyecto basado en la documentación compartida por Jorge Aliaga - http://jorgealiaga.com.ar/?page_id=2864 -, con el fin de aprender un poco mas y poder compartir un proyecto completo.

Al proyecto anteriormente nombrado se le agrega:

    -Módulo WifiManager - con el fin de tener un portal cautivo para configurar la WiFi de manera sencilla

    -Módulo EEPROM para guardar datos de manera permanente 

    -La pantalla OLED utilizada requeria módulo SH1106

    -Módulo ESP8266HTTPClient, para poder enviar datos de las mediciones a internet o a servidor (se tomo como referencia thingspeak por ser software libre)

    -Botón de reset, borra las configuraciones de WiFI y de envío de datos

    -Modelo en Fritzing para poder imprimir placa estandar de 5x5 y soldar los integrados.

    -Modelo de caja para imprimir en 3d, realizada con FreeCad

    -A la pagina web de consulta de datos se le agregaron las opciones de calibración , volver a fábrica y configurar servidor y clave de transmisión de envío de datos


## Pequeña descripción

Comencé probando con easyesp y arme una versión sencilla y rápida de detector de CO2, si bien conocía otros proyectos me incliné por esa opción

Una vez que funcionó , decidí hacer algo que pueda aportar al proyecto anterior, pero considero que  no tenia experiencia, por lo tanto lo tomé como un desafío que puede llegar a ser útil.  

Este proyecto me llevo bastante tiempo y me sirvió para entender un poco mas sobre esp8266, primeros pasos con electrónica y “planchado” de placas, diseño 3d sencillo con Freecad y divertirme nerdeando… :) . 


Como todo trabajo no es individual, no lo podría haber realizado sin Martín Morales que me prestó el sensor detector de CO2, Norberto Pellegrini que me explicó un montón de cosas del armado de placas (me falta aprender muuucho y más práctica) y me presto el LCD para la primera versión.


## Advertencia: aún está en fase experimental, requiere muchas mejoras 
