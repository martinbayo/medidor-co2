/*Codigo liberado bajo GPL v3 */


// Librerias Pantalla SH1106
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SH110X.h>

// Librerias de Texto
#include <Fonts/FreeSerif9pt7b.h>

// incluye librería para manejo del sensor de CO2
#include <MHZ19_uart.h> 

// manejo de wifi de wemos
#include <ESP8266WiFi.h>  

// manejo de lectura de cliente web
#include <ESP8266HTTPClient.h>

// manejo de portal captivo y configuracion wifi
#include <WiFiManager.h> // https://github.com/tzapu/WiFiManager

//manejo de guardado permanente
#include <EEPROM.h>


// Inicializa pantalla
/* Uncomment the initialize the I2C address , uncomment only one, If you get a totally blank screen try the other*/
#define i2c_Address 0x3c //initialize with the I2C addr 0x3C Typically eBay OLED's
//#define i2c_Address 0x3d //initialize with the I2C addr 0x3D Typically Adafruit OLED's

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels
#define OLED_RESET -1   //   QT-PY / XIAO
Adafruit_SH1106G display = Adafruit_SH1106G(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);


#define NUMFLAKES 10
#define XPOS 0
#define YPOS 1
#define DELTAY 2


WiFiServer server(80);  // crea un servicio que escucha conecciones entrantes de un puerto específico

// define pines de mhz9 y buzzer
const int rx_pin = 14;  //Serial rx pin no 14
const int tx_pin = 16; //Serial tx pin no 16
const int buzzer = D3 ;
int cnt = 0; // cuenta LOOPS

// define los leds
const int led_R = 12 ; // control LED ROJO 12
const int led_V = 13 ; // control LED VERDE 13
const int led_A = 15 ; // control LED AZUL
boolean anodoComun0 = false;   // Led RGB (ánodo común)

const int cal_pin = 2;  // entrada pulsador calibración

const int analogInPin = A0;  // Entrada pulsador Reset

MHZ19_uart mhz19; // asigna medidor al sensor
int co2ppm;

WiFiManager wm;

int sensorValue = 0;

// definicion de datos de thingspeak
//String apiKey = "TDEU3YCGZ0KNY4W9";
//const char* log_server = "api.thingspeak.com";
String server_log;
String server_log_key;
//variables para guardar en eeprom
String datoaguardar;
void writeString(char add,String data);
String read_String(char add);
String recivedData;



WiFiClient client;

void setup() {
   
  Serial.begin(115200); // abre puerto serie para comunicarse

  pinMode(led_R, OUTPUT);  //Inicia LED ROJO
  pinMode(led_V, OUTPUT);  //Inicia LED VERDE
  pinMode(led_A, OUTPUT);  //Inicia LED AZUL
  definirColor(0, 255, 0,anodoComun0);  // empieza con LED en verde

  pinMode(cal_pin, INPUT); // entrada pulsado para calibrar


  EEPROM.begin(512);

   // empieza programa de medición
  pinMode(buzzer, OUTPUT); //Inicia buzzer, pita para avisar que arrancó
  tone(buzzer, 55, 20);

    // Inicializar el display
  display.begin(i2c_Address, true); // Address 0x3C default
 //display.setContrast (0); // dim display
 
  display.display();
  delay(2000);

  // Clear the buffer.
  display.clearDisplay();

  delay(10);
  
  // Define tamaño de texto 
  display.setTextSize(0);
  display.setTextColor(SH110X_WHITE);
  display.setCursor(0, 0);

   // Escribimos Mensaje de inicio en el display.
  display.setTextColor(SH110X_WHITE);
  display.setCursor(30, 30); // Ubicamos el cursor en la primera posición(columna:0) de la segunda línea(fila:1)
  display.println("INICIANDO");
  Serial.println("INICIANDO");
  display.display();
  delay(5000); // espera 5 segundos
  display.clearDisplay(); // borra pantalla

/*----------------------------------------------------------
    calentando MH-Z19 CO2 sensor
  ----------------------------------------------------------*/

  mhz19.begin(rx_pin, tx_pin); // inicializa el sensor
  mhz19.setAutoCalibration(false); // deshabilita autocalibrado automático
  
// muestra indicación de que está calentando 
  display.setCursor(0, 30); // Ubicamos el cursor en la primera posición(columna:0) de la primera línea(fila:0)
  display.print("Calentando");    // Escribe primera linea del cartel
  display.setCursor(0, 40); // Ubicamos el cursor en la primera posición(columna:0) de la segunda línea(fila:0)
  display.print("Espere 1 minuto");  // Escribe segunda linea del cartel
  display.display();

  delay(60000); // espera 1 minuto
  display.clearDisplay(); // borra pantalla  
  display.display();

  tone(buzzer, 55,100); // pitido de 55Hz indicando que funciona normal, calentado

// toma datos de server_log en memoria
  datoaguardar = read_String(256);
  Serial.print("Read Data:");
  Serial.println(datoaguardar);
  int divisor = datoaguardar.indexOf('!');
  server_log = datoaguardar.substring(0,divisor);
  server_log_key = datoaguardar.substring(divisor + 1);
  server_log_key.trim();

 
//define WIFI 

  WiFi.mode(WIFI_STA); // explicitly set mode, esp defaults to STA+AP


//WiFiManager, Local intialization. Once its business is done, there is no need to keep it around
  WiFiManager wm;
  //wm.setConnectTimeout(30);
  display.clearDisplay(); // borra pantalla  
  display.setCursor(0,0);
  display.print("Configuracion wifi"); //
  display.setCursor(0,10); // Ubicamos el cursor en la primera posición(columna:0) de la segunda línea(fila:1)
  display.print("DEMORA 90 SEG");    // Escribe texto
  display.setCursor(0,20);
  display.print("SSID: MedidorCO2"); //
  display.setCursor(0,30); // Ubicamos el cursor en la primera posición(columna:0) de la segunda línea(fila:1)
  display.print("Password: password");    // Escribe texto
  
  display.display();
  
  wm.setConfigPortalTimeout(90);
  bool res;
  // res = wm.autoConnect(); // auto generated AP name from chipid
  // res = wm.autoConnect("AutoConnectAP"); // anonymous ap
  res = wm.autoConnect("MedidorCO2","password"); // password protected ap

}

// prepara página web para mandar a un cliente (servidor web)
String prepareHtmlPage()
{
  String htmlPage;
  htmlPage.reserve(1024);               // prevent ram fragmentation
  htmlPage = F("HTTP/1.1 200 OK\r\n"
               "Content-Type: text/html\r\n"
               "Connection: close\r\n"  // the connection will be closed after completion of the response
               "Refresh: 5\r\n"         // refresh the page automatically every 5 sec
               "\r\n"
               "<!DOCTYPE HTML>"
               "<html>"
               "<h1>"
               "<p> Medidor de CO<sub>2</sub></p>"
               "<h2>"
               "co2:  ");
  htmlPage += co2ppm; // imprime el valor de CO2
  htmlPage += F("ppm.<br />"
                "IP: ");
  htmlPage += WiFi.localIP().toString().c_str(); // imprime el valor IP
  htmlPage += F("<br />"
                "<br />"
                "<hr> "
                " Seccion De Conexion a servidor de logs"
                "<br />"
                "</h2>"
                "Tiene conexion por defecto a thingspeak.com , que es Software Libre y se puede instalar en un servidor propio."
                "<br />"
                "<br />"
                "<form action=/action_page>"
                "Servidor de log: <input type=\"text\" name=\"Sevidorlog\"  value=\"api.thingspeak.com\">"
                "<br />"
                "Clave de transmision: <input type=\"text\" name=\"Clavedeapi\" >"
                "<br />"
                "<input type=\"submit\" value=\"Submit\">"
                "</form><br>"
                "<br />" 
                "<hr> "
                "<h2> Calibrar</h2>"
                "<br />"
                "Boton para calibrar el equipo, para ello es necesario accionarlo estando con el equipo 30 minutos en el exterior"
                "<br />"
                "<button onClick=location.href='./?Calibrar'>Calibrar</button>"
                "<br />"
                "<br />" 
                "<hr> "
                "<h2> Volver a Fabrica</h2>"
                "<br />"
                "<button onClick=location.href='./?Reset'>Reset</button>"
                "Borra las condiciones de WiFI y de servidor de logs"
                "</html>"
                "\r\n");
  return htmlPage;
}

// rutina para manejar colores de leds
void definirColor(int rojo, int verde, int azul, boolean anodoComun)
{
  if(anodoComun == true)
  {
    rojo = 255 - rojo;
    verde = 255 - verde;
    azul = 255 - azul;
  }

  analogWrite(led_R, rojo); // Prende LED ROJO
  analogWrite(led_V, verde); // Prende LED VERDE
  analogWrite(led_A, azul); // Prende LED AZUL
}

void resetwifi()
{
  wm.resetSettings();
  server_log = "0";
  server_log_key = "0";
  datoaguardar = server_log + "!" + server_log_key;
  writeString(256, datoaguardar); 
  ESP.restart();
  }

void calibrar()
{
  const int waitingMinutes = 30;  //cantidad de minutos a esperar
  const long waitingSeconds = waitingMinutes * 60L; // cantidad de segundos a esperar
  long cnt_cal = 0; // cuenta segundos de calibración
  definirColor(0, 0, 255,anodoComun0); // PRENDE LED AZUL

// indica que empieza la calibración, demnora y donde ubicar el equipo  
  display.clearDisplay(); // borra pantalla  
  display.setCursor(0,20);
  display.print("CALIBRACION"); //
  display.setCursor(0,30); // Ubicamos el cursor en la primera posición(columna:0) de la segunda línea(fila:1)
  display.print("DEMORA 30 MIN");    // Escribe texto
  display.display();
  delay(30000); // Espera 30 segundos

  display.clearDisplay(); // borra pantalla  
  display.setCursor(0,20);
  display.print("PONER EL EQUIPO"); //
  display.setCursor(0, 30); // Ubicamos el cursor en la primera posición(columna:0) de la segunda línea(fila:1)
  display.print("AL AIRE LIBRE");    // Escribe texto
  display.display();
  delay(30000); // Espera 30 segundos

  display.clearDisplay(); // borra pantalla  
  display.setCursor(0,20);
  display.print("COMIENZA"); //
  display.setCursor(0,30); // Ubicamos el cursor en la primera posición(columna:0) de la segunda línea(fila:1)
  display.print("CALIBRACION"); //
  display.display();
  delay(10000); // Espera 10 segundos

  while (cnt_cal <= waitingSeconds) { // espera media hora
    if ( ++cnt_cal % 60 == 0) { // Si cnt es múltiplo de 60
      display.clearDisplay(); // borra pantalla  
      display.setCursor(0,20);
      display.print(cnt_cal / 60); // Cada minuto muestra el valor
      display.setCursor(0,30);
      display.print("minutos"); // 
      display.setCursor(0, 40); // Ubicamos el cursor en la primera posición(columna:0) de la segunda línea(fila:1)
      display.print("co2: ");    // Escribe texto
      display.setCursor(20,40); // Ubicamos el cursor en la novena posición(columna:8) de la segunda línea(fila:1)
      display.print(mhz19.getPPM()); // Escribe CO2
      display.setCursor(60,40); // Ubicamos el cursor en la treceava posición(columna:12) de la segunda línea(fila:1)
      display.print("ppm"); // Escribe texto
      display.display();
    } else {
      display.clearDisplay(); // borra pantalla  
      display.setCursor(0,20);
      display.print("CALIBRANDO"); //
      display.setCursor(0,30);
      display.print(cnt_cal); // muestra los segundos transcurridos
      display.setCursor(20,30);
      display.print("segundos");    
      display.display();
    }
    delay(1000); // Espera 1 segundo
  }
  // paso media hora  
  display.clearDisplay(); // borra pantalla  
  mhz19.calibrateZero(); // Calibra
  display.setCursor(0,20);
  display.print("PRIMERA"); //
  display.setCursor(0,30); // Ubicamos el cursor en la primera posición(columna:0) de la segunda línea(fila:1)
  display.print("CALIBRACION");    // Escribe texto
  display.display();
  
  delay(60000); // Espera 60 segundos
  display.clearDisplay(); // borra pantalla  
  mhz19.calibrateZero();  // Calibra por segunda vez por las dudas
  display.setCursor(0,20);
  display.print("SEGUNDA"); //
  display.setCursor(0,30); // Ubicamos el cursor en la primera posición(columna:0) de la segunda línea(fila:1)
  display.print("CALIBRACION");    // Escribe texto
  display.display();
  delay(10000); // Espera 10 segundos 

  display.clearDisplay(); // borra pantalla  
  display.setCursor(0,20);
  display.print("CALIBRACION");    // Escribe texto
  display.setCursor(0, 30); // Ubicamos el cursor en la primera posición(columna:0) de la segunda línea(fila:1)
  display.print("TERMINADA");    // Escribe texto
  display.display();
  delay(10000); // Espera 10 segundos 

}


void writeString(char add,String data)
{
  int _size = data.length();
  int i;
  for(i=0;i<_size;i++)
  {
    EEPROM.write(add+i,data[i]);
  }
  EEPROM.write(add+_size,'\0');   //Add termination null character for String Data
  EEPROM.commit();
}


String read_String(char add)
{
  int i;
  char data[100]; //Max 100 Bytes
  int len=0;
  unsigned char k;
  k=EEPROM.read(add);
  while(k != '\0' && len<500)   //Read until null character
  {    
    k=EEPROM.read(add+len);
    data[len]=k;
    len++;
  }
  data[len]='\0';
  return String(data);
}

void loop() {
  co2ppm = 0;

  if (digitalRead(cal_pin) == LOW) { // si detecta el botón de calibrar apretado, calibra
      calibrar();
  }

   if ( ++cnt % 30 == 0) { // Si cnt es múltiplo de 30
    display.clearDisplay(); // borra pantalla  
    display.setCursor(0,30);
    display.println("MEDIDOR CO2"); // Cada minuto muestra leyenda institucional
    display.display();

    delay(3000); //demora 3 seg entre mediciones
    cnt = 0; // vuelve a empezar LOS LOOPS
  } else {

  }
  
  co2ppm = mhz19.getPPM(); // mide CO2

//  Muestra medición de CO2    
  display.clearDisplay(); // borra pantalla
  display.setCursor(35,0);
  display.print("MEDIDOR CO2"); // 
  display.setCursor(0, 20); // Ubicamos el cursor en la primera posición(columna:0) de la primera línea(fila:0)
  display.print("CO2: ");    // Escribe texto
  display.setCursor(50, 20); // Ubicamos el cursor en la novena posición(columna:8) de la primera línea(fila:0)
  display.print(co2ppm); // Escribe CO2
  display.setCursor(90, 20); // Ubicamos el cursor en la treceava posición(columna:12) de la primera línea(fila:0)
  display.print("ppm"); // Escribe texto
  display.display();

  if (WiFi.status() == WL_CONNECTED) { // está conectado a wifi
    display.setCursor(0, 40); // Ubicamos el cursor en la primera posición(columna:0) de la primera línea(fila:1)
    display.print("WIFI conectado"); // AVISA QUE SE CONECTÓ A WIFI
    display.setCursor(0, 50); // Ubicamos el cursor en la primera posición(columna:0) de la primera línea(fila:1)
    display.print( WiFi.localIP().toString().c_str()); // MUESTRA IP de WIFI
    display.display();
//    delay(10000); // espera 5 segundos
//    display.clearDisplay(); // borra pantalla
    
    Serial.println("");
    Serial.println("WiFi conectado"); // avisa en el puerto serie que está conectado
    // Print the IP address
    Serial.printf("Servidor Web Iniciado, abra %s en un mavegador web\n", WiFi.localIP().toString().c_str()); // avisa que se abrió la el puerto y da el número

    server.begin();
    
    Serial.println("Por transmitir.");
    Serial.println(co2ppm);

   // Se Envia datos al servidor de log
    if (client.connect(server_log,80)) {
      String postStr = server_log_key;
      postStr +="&field1=";
      postStr += String(co2ppm);
      postStr += "\r\n\r\n";
 
      client.print("POST /update HTTP/1.1\n");
      client.print("Host: api.thingspeak.com\n");
      client.print("Connection: close\n");
      client.print("X-THINGSPEAKAPIKEY: "+server_log_key+"\n");
      client.print("Content-Type: application/x-www-form-urlencoded\n");
      client.print("Content-Length: ");
      client.print(postStr.length());
      client.print("\n\n");
      client.print(postStr);
    } 
//client.stop();
    
    
  } else {
    display.setCursor(0, 50); // Ubicamos el cursor en la primera posición(columna:0) de la primera línea(fila:1)
    display.print("WIFI NO conectado"); // AVISA QUE SE CONECTÓ A WIFI
    display.display();
    Serial.println("");
    Serial.println("WiFi NO conectado"); // avisa en el puerto serie que está conectado
    
  }


//  Emite un sonido en función del resultado
    int tono = co2ppm / 100;
    switch (tono){
    case 3: // menor a 600ppm, 55Hz, 0.1 seg
       display.setCursor(0, 1); // Ubicamos el cursor en la primera posición(columna:0) de la segunda línea(fila:1)
       display.print("RIESGO BAJO");    // Escribe texto
       definirColor(0, 50, 0,anodoComun0); // PRENDE LED VERDE
       break;
    case 4: // menor a 600ppm, 55Hz, 0.1 seg
       display.setCursor(0, 1); // Ubicamos el cursor en la primera posición(columna:0) de la segunda línea(fila:1)
       display.print("RIESGO BAJO");    // Escribe texto
       definirColor(0, 50, 0,anodoComun0); // PRENDE LED VERDE
       break;
    case 5: // menor a 600ppm, 55Hz, 0.1 seg
//       tone(buzzer, 55,100);
       display.setCursor(0, 1); // Ubicamos el cursor en la primera posición(columna:0) de la segunda línea(fila:1)
       display.print("RIESGO BAJO");    // Escribe texto
       definirColor(0, 50, 0,anodoComun0); // PRENDE LED VERDE
       break;
    case 6: // menor a 700ppm, 55Hz, 0.1 seg// menor a 600ppm
//       tone(buzzer, 55,100);    
       display.setCursor(0, 1); // Ubicamos el cursor en la primera posición(columna:0) de la segunda línea(fila:1)
       display.print("RIESGO BAJO");    // Escribe texto
       definirColor(0, 50, 0,anodoComun0); // PRENDE LED VERDE
       break;
    case 7: // menor a 800ppm, 55Hz, 0.1 seg
       tone(buzzer, 55,100);
       display.setCursor(0, 1); // Ubicamos el cursor en la primera posición(columna:0) de la segunda línea(fila:1)
       display.print("RIESGO MEDIO");    // Escribe texto
       definirColor(255, 50, 0,anodoComun0); // PRENDE LED AMARILLO
       break;
    case 8: // menor a 900ppm, 110Hz, 1 seg
       tone(buzzer, 110,1000);
       display.setCursor(0, 1); // Ubicamos el cursor en la primera posición(columna:0) de la segunda línea(fila:1)
       display.print("RIESGO ALTO");    // Escribe texto       
       definirColor(255, 0, 0,anodoComun0); // PRENDE LED ROJO
       break;
    case 9: // menor a 1000ppm, 220Hz, 2 seg
       tone(buzzer, 220,2000);
       display.setCursor(0, 1); // Ubicamos el cursor en la primera posición(columna:0) de la segunda línea(fila:1)
       display.print("RIESGO ALTO");    // Escribe texto
       definirColor(255, 0, 0,anodoComun0); // PRENDE LED ROJO
       break;
    case 10: // menor a 1100ppm, 440Hz, 2 seg
       tone(buzzer, 440,2000); 
       display.setCursor(0, 1); // Ubicamos el cursor en la primera posición(columna:0) de la segunda línea(fila:1)
       display.print("RIESGO ALTO!!");    // Escribe texto
       definirColor(255, 0, 0,anodoComun0); // PRENDE LED ROJO
       break;
    case 11: // menor a 120ppm, 880Hz, 2 seg
       tone(buzzer, 880,2000); 
       display.setCursor(0, 1); // Ubicamos el cursor en la primera posición(columna:0) de la segunda línea(fila:1)
       display.print("RIESGO ALTO!!");    // Escribe texto
       definirColor(255, 0, 0,anodoComun0); // PRENDE LED ROJO
       break;
    default: // menor a 1300ppm, 880Hz, 4 seg
       tone(buzzer, 880,4000);
       display.setCursor(0, 1); // Ubicamos el cursor en la primera posición(columna:0) de la segunda línea(fila:1)
       display.print("RIESGO ALTO!!!!");    // Escribe texto
       definirColor(255, 0, 0,anodoComun0); // PRENDE LED ROJO
       break;
    }
   
  Serial.println(co2ppm); // escribe en el puerto serie el valor de CO2

// INICIO PARTE PARA cliente web wifi 
  if (WiFi.status() == WL_CONNECTED) {
  WiFiClient client = server.available();
  // wait for a client (web browser) to connect
  if (client)
  {
//    Serial.println("\n[Client connected]");
    String linea1 = client.readStringUntil('\r');
    Serial.println(linea1);
    //String linea2 = client.readString();
    //Serial.println(linea2);
    if (linea1.indexOf("Reset")>0)
        {
         resetwifi();
    }
    if (linea1.indexOf("Calibrar")>0)
        {
         calibrar();
    }
    if (linea1.indexOf("Sevidorlog")>0)
        {
         int datoinicioserver = linea1.indexOf('=');
         int datofinserver = linea1.indexOf('&');
         String linea2 = linea1.substring(datoinicioserver+1);
         int datoinicioclave = linea2.indexOf('=');
         int datofinclave = linea2.indexOf('H');
         server_log = linea1.substring(datoinicioserver + 1,datofinserver);
         server_log_key = linea2.substring(datoinicioclave + 1,datofinclave);
         server_log_key.trim();
         datoaguardar=server_log + "!" + server_log_key;

          //graba en eeprom
         writeString(256, datoaguardar);
         
        }
    while (client.connected())
        {
        
      // read line by line what the client (web browser) is requesting
      if (client.available())
      {
        String line = client.readStringUntil('\r');
//*        Serial.print(line);
        // wait for end of client's request, that is marked with an empty line
        if (line.length() == 1 && line[0] == '\n')
        {
          client.println(prepareHtmlPage());
          break;
        }
      }
    }

    while (client.available()) {
      // but first, let client finish its request
      // that's diplomatic compliance to protocols
      // (and otherwise some clients may complain, like curl)
      // (that is an example, prefer using a proper webserver library)
      client.read();
    }

    // close the connection:
    client.stop();
//    Serial.println("[Client disconnected]");
  }
  }
// FIN PARTE PARA WIFI  */

    // se verifica si se presiono boton de reset wifi
    sensorValue = analogRead(analogInPin);
    if (sensorValue == 1024) {
        resetwifi();
    }
    
  delay(5000); //demora 10 seg entre mediciones

}
